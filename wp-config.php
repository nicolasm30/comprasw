<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'comprasw');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'root');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'sN9)<:$P(-zR(+*!4VKS2eVLD4^K1wQ{|Km{VcY(6]Qw9<]lo{EdyPt2+RlN=N^g');
define('SECURE_AUTH_KEY', 'w(O>rS}V~XS94?qjgIFCb`YEha$:ftC`K<xDbg0t^-yGSYU0C%@a;w35Z/#JsQ@G');
define('LOGGED_IN_KEY', 'Qz2CHwi]_p;4/kkgws`O4QKX46Gr9G{U{djd86d_N2xFJ%LG0}Hzf]hOir=:vA<V');
define('NONCE_KEY', 'zw_!;c=>[Cm1srX3 1VqMSrNmtO8TpPJ1DACV2Sns-GX]p?t&Dt+#}V+ Sh_2Dwg');
define('AUTH_SALT', 'v`|aW;`-xmYj_>+vFXf.ZFElZ(ZvPH1YpUuErc@Jz* CC5KC{LjkOyl{!9pZ+@f9');
define('SECURE_AUTH_SALT', 'h1?7uRw!~I(; *i$;ozn1)NinS!F_:e/NE[L-y36n*EI,09 Y>?T~|AY,l$GZLS_');
define('LOGGED_IN_SALT', '~|z&=:M4-!J[W3g3k{JH^>$Jc&H}XMb|+I:Qesl[$ n@-/Iu(P!(Mqy|)VGk<}f*');
define('NONCE_SALT', '+?UC]u|3lS.9*Q`Az w&eoD;Q2*+wx4?*}uz#9Mj4?NvH *(K Ti}yp2jnw8;rjm');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'com_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

