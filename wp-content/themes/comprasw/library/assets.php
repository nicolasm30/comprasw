
<?php 
/*:::::::::::::CSS y JS del tema:::::::::::::::::*/
function mainlinks()
{
    // Register styles:
	wp_register_style( 'main_style', get_template_directory_uri() . '/css/main.css');
	wp_register_style( 'main_style2', get_template_directory_uri() . '/css/main2.css');
	wp_register_style( 'responsive', get_template_directory_uri() . '/css/responsive.css');
	wp_register_style( 'fonts', get_template_directory_uri() . '/css/fonts.css');
	wp_register_style( 'owl', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css');
	wp_register_style( 'bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
	wp_register_style( 'opensans', 'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap');
	wp_register_style( 'Montserrat', 'https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i&display=swap');

	 // Register Scripts:
    wp_register_script( 'main_jquery', get_template_directory_uri() . '/js/jquery.js');
	wp_register_script( 'mainscript', get_template_directory_uri() . '/js/mainscript.js' );
	wp_register_script( 'owl', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js' );
	wp_register_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' );
	wp_register_script( 'bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' );

	// Add Styles ↓↓:
	wp_enqueue_style( 'main_style' );
	wp_enqueue_style( 'main_style2' );
	wp_enqueue_style( 'responsive');
	wp_enqueue_style( 'fonts');
	wp_enqueue_style( 'owl' );
	wp_enqueue_style( 'bootstrap' );
	wp_enqueue_style( 'opensans' );
	wp_enqueue_style( 'Montserrat' );
	
	// Add Scripts ↓↓:
	wp_enqueue_script( 'main_jquery');
	wp_enqueue_script( 'mainscript' );
	wp_enqueue_script( 'owl' );
	wp_enqueue_script( 'popper' );
	wp_enqueue_script( 'bootstrap' );
}
add_action( 'wp_enqueue_scripts', 'mainlinks', 5 );//el 5 es la prioridad para no tener conflictos con js genericos de wordpress

/*==========================================================================*/

?>