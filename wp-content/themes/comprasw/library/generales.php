<?php
/*==============menus y widgets========================*/

add_theme_support('nav-menus');

if(function_exists ('register_nav_menus')){
   register_nav_menus(
        array(
            'header-menu' => __( 'Header Menu' ),
            'footer-menu' => __( 'Footer Menu' )
          )
   );
}


/*widgets*/
function widgets_de_tema() {

    register_sidebar( array(
        'name'          => 'widget de sidebar',
        'id'            => 'widget_1',
        'before_widget' => '<div class="widgetdiv">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="titulowidget">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => 'Sidebar Productos',
        'id'            => 'side_productos',
        'before_widget' => '<div class="sideproductos">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="Categorias">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => 'Buscador',
        'id'            => 'buscador',
        'before_widget' => '<div class="buscador">',
        'after_widget'  => '</div>',
    ) );
}
add_action( 'widgets_init', 'widgets_de_tema' );
/*======================================*/

/* Functions Generales  */
add_filter( 'the_title', 'shorten_woo_product_title', 10, 2 );
function shorten_woo_product_title( $title, $id ) {
    if ( get_post_type( $id ) === 'product' ) {
        return wp_trim_words( $title, 4 );
    } else {
        return $title;
    }
}

	/**
 * Grab latest post title by an author!
 *
 * @param array $data Options for the function.
 * @return string|null Post title for the latest,  * or null if none.
 */
function get_data_product($data) {
    $category = (int)$data['category'];
    $args = array(
        'post_type'             => 'product',
        'post_status'           => 'publish',
        'posts_per_page'        => '12',
        'tax_query'             => array(
            array(
                'taxonomy'      => 'product_cat',
                'field' => 'term_id',
                'terms'         => $category,
                'operator'      => 'IN'
            )
        )
    );
    $products = new WP_Query($args);
    foreach ($products->posts as $pro) {
        $pro->thumbnail = get_the_post_thumbnail($pro->ID);
        $pro->price = get_post_meta( $pro->ID, '_regular_price', true);
        $pro->url = get_permalink($pro->ID);
    }
    return $products->posts;
  }
function get_all_product() {
    $args = array(
        'post_type'             => 'product',
        'post_status'           => 'publish',
        'posts_per_page'        => '20'
    );
    $allproducts = new WP_Query($args);
    
    foreach ($allproducts->posts as $allpro) {
        $allpro->thumbnail = get_the_post_thumbnail($allpro->ID);
        $allpro->price = get_post_meta( $allpro->ID, '_regular_price', true);
        $allpro->url = get_permalink($allpro->ID);
    }
    return $allproducts->posts;
  }
  add_action( 'rest_api_init', function () {
	register_rest_route( 'producto', '/(?P<category>\d+)', array(
	  'methods' => 'GET',
	  'callback' => 'get_data_product',
	) );
	register_rest_route( 'productos','/all',array(
	  'methods' => 'GET',
	  'callback' => 'get_all_product',
	) );
  } );