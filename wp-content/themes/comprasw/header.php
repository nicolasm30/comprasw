<header>
    <div class="contheader">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/Header/logo.png" alt="">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="search">
                    <?php if ( is_active_sidebar( 'buscador' ) ) : ?>
                        <?php dynamic_sidebar( 'buscador' ); ?>
                    <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="car-search-soc">
                        <div class="car">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/Header/carro.png" alt=""> Carro de Compras
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="contmenu">
        <nav>
            <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
        </nav>
    </div>

    <!-------------- prueba ------------->
    <div class="menu-mobil">
        <div class="menu_bar">
            <a href="#" class="bt-menu"><span class="icon-menu"></span>Menu</a>
        </div>
 
        <nav>
            <ul>
                <li><a href="#"><span class="icon-house"></span>INICIO</a></li>
                <li><a href="#"><span class="icon-suitcase"></span>COMPRAS BAJO PEDIDO</a></li>
                <li><a href="#"><span class="icon-rocket"></span>PRODUCTOS</a></li>
                <li><a href="#"><span class="icon-earth"></span>OFERTAS</a></li>
                <li><a href="#"><span class="icon-mail"></span>IMPORTACIONES</a></li>
                <li><a href="#"><span class="icon-mail"></span>CONTACTOS</a></li>
            </ul>
        </nav>
    </div>
</header>
<script>
    $(document).ready(main);

var contador = 1;

function main(){
    $('.menu_bar').click(function(){
        // $('nav').toggle(); 

        if(contador == 1){
            $('nav').animate({
                left: '0'
            });
            contador = 0;
        } else {
            contador = 1;
            $('nav').animate({
                left: '-100%'
            });
        }

    });

};
</script>