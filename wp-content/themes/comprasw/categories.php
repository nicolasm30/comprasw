<h3 class="title_categories">Categorías</h3>
<?php  
$categories = get_terms();
echo "<ul class='categories'>";
foreach ($categories as $category) {
    echo "<li id='category-$category->term_id' data-category='$category->term_id'>" . $category->name . "</li>"; 
}
echo "</ul>";
?>
