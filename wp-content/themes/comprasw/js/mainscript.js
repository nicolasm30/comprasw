$(document).ready(function(){
    var url = "http://localhost:8080/comprasw";
    $.ajax({
        url: url+'/wp-json/productos/all',
        success: function(respuesta) {
            $('.products').empty();
            respuesta.forEach(producto => {
                var template = `
                    <li class="productos col-md-4">
                        <a href="`+producto.url+`">
                        `+producto.thumbnail+`
                        <h4>`+producto.post_title+`</h4>
                        <span class="price">$`+producto.price+`</span>
                        </a>
                    </li>
                `
                $('.products').append(template);
            });
            console.log("entro");
        },
        error: function() {
            console.log("No se ha podido obtener la información");
        }
    });
    $('.products_destacados').owlCarousel({
        loop:true,
        nav:true,
        margin:100,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:4
            }
        }
    });
    var url = "http://localhost:8080/comprasw";
    $(".categories li").on('click', function(){
        $(".categories li.active").removeClass('active');
        $(this).addClass('active');
    var id = $(this).attr('data-category');
        $.ajax({
            url: url+'/wp-json/producto/'+id,
            success: function(respuesta) {
                $('.products').empty();
                respuesta.forEach(producto => {
                    var template = `
                        <li class="productos col-md-4">
                            <a href="`+producto.url+`">
                            `+producto.thumbnail+`
                            <h4>`+producto.post_title+`</h4>
                            <span class="price">$`+producto.price+`</span>
                            </a>
                        </li>
                    `
                    $('.products').append(template);
                });
                console.log("entro");
            },
            error: function() {
                console.log("No se ha podido obtener la información");
            }
        });
    });
});
// function productos(){
//     foreach ($respuesta as pro){

//     }
// }