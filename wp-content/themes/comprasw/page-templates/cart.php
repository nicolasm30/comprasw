<?php
/*=================
Template Name: Carro
===================*/
get_header('wordpress');
 ?>
 <div class="container cart">
    <div class="row">
        <div class="col">        
            <?php echo do_shortcode('[woocommerce_cart]'); ?>
        </div>
    </div>
 </div>
<?php get_footer(); ?>
