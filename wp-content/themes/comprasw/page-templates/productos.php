<?php
/*=================
Template Name: Productos
===================*/
get_header('wordpress'); ?>
<div class="container-fluid productosPage">
    <div class="container">
        <div class="row all-products">
            <div class="col-md-4">
				<?php include (TEMPLATEPATH . '/categories.php'); ?>
			</div>
			<div class="col-md-8">
				<ul class="products">
				</ul>
			</div>
        </div>
    </div>
</div>

<?php get_footer(); ?>