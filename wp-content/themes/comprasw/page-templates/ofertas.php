<?php
/*=================
Template Name: Ofertas
===================*/
get_header('wordpress'); ?>
<div class="container-fluid ofertas">
    <div class="row">
        <div class="banner_contacto">
            <img src="<?php echo get_template_directory_uri(); ?>/img/ofertas/bn-ofertas.jpg" alt="">
        </div>
    </div>

    <div class="container">
        <div class="row all-products">
            <div class="col-md-4">
				<?php include (TEMPLATEPATH . '/categories.php'); ?>
			</div>
			<div class="col-md-8">
				<ul class="products">
				</ul>
			</div>
        </div>
    </div>
</div>

<?php get_footer(); ?>