<?php
/*=================
Template Name: Compras bajo pedido
===================*/
get_header('wordpress'); ?>

<div class="background">
	<section class="container title-principal text-center pt-5">
		<div class="row">
			<div class="col-12">
				<h1>Déjanos saber que quieres comprar (cualquier producto), que nosotros realizaremos la compra en cualquier parte del mundo y te lo entregaremos donde quieras!</h1>
				<hr>
			</div>
		</div>
	</section>


	<div class="container co-general pt-5 pb-5">
		<div class="row">
		  <div class=" col-sm-12 col-md-12 col-lg-2">
		    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
		      <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">COMO COMPRAR</a>
		      <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Donde COMPRAR<br> (Tiendas Sugeridas)</a>
		      <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false"><strong>Solicitud de Cotización</strong></a>
		      <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Quienes somos</a>
		    </div>
		  </div>
		  <div class="col-sm-12 col-md-12 col-lg-10">
		    <div class="tab-content" id="v-pills-tabContent">
		      <!-------- primer tab -------->
		      <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
		      	<h2>Como COMPRAR:</h2>
		      	<div class="row co-pasos pt-5">
		      		<div class="cont-comprar col-sm-12 col-md-12 col-lg-6">
		      			<div class="row">
			      			<div class="col-12 cabezote">
					      		<img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/1.png">
					      		<p>Define el producto que quieres comprar</p>
				      		</div>
				      		<div class="col-12 im-cabezote">
				      			<img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/define-el-producto.png" alt="Define el producto">
				      		</div>
			      		</div>
		      		</div>
		      		<div class="cont-comprar col-sm-12 col-md-12 col-lg-6">
		      			<div class="row">
			      			<div class="col-12 cabezote">
					      		<img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/2.png">
					      		<p>Busca el producto que quieres comprar ingresando a la tienda online donde lo venden (Ver lista de tiendas online sugeridas en: DONDE COMPRAR - en el menú lado izq.).</p>
				      		</div>
				      		<div class="col-12 im-cabezote">
				      			<img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/buequeda-del-producto.png" alt="busqueda del producto">
				      		</div>
			      		</div>
		      		</div>
		      	</div>

		      	<div class="row co-pasos pt-5">
		      		<div class="cont-comprar col-sm-12 col-md-12 col-lg-6">
		      			<div class="row">
			      			<div class="col-12 cabezote">
					      		<img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/3.png">
					      		<p>Copia (Ctrl + C) el enlace (link) del producto que quieres comprar.</p>
				      		</div>
				      		<div class="col-12 im-cabezote">
				      			<img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/enlace.png" alt="copiar el enlace">
				      		</div>
			      		</div>
		      		</div>
		      		<div class="cont-comprar col-sm-12 col-md-12 col-lg-6">
		      			<div class="row">
			      			<div class="col-12 cabezote">
					      		<img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/4.png">
					      		<p>Diligencia el formulario Solicitud de Cotización (en SOLICITUD DE COTIZACION – en el menú lado izq.), y pegua (Ctrl + V) el enlace (link) del producto que quieres comprar en la sección producto a cotizar.</p>
				      		</div>
				      		<div class="col-12 im-cabezote">
				      			<img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/diligencia-formulario.png" alt="Diligencia el formulario">
				      		</div>
			      		</div>
		      		</div>
		      	</div>

		      	<div class="row co-pasos pt-5">
		      		<div class="cont-comprar col-sm-12 col-md-12 col-lg-6">
		      			<div class="row">
			      			<div class="col-12 cabezote">
					      		<img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/5.png">
					      		<p>Nosotros te cotizaremos el producto (con pago de impuestos), al mejor precio que en el mercado nacional.</p>
				      		</div>
				      		<div class="col-12 im-cabezote">
				      			<img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/cotizaremos-el-producto.png" alt="te cotizaremos el producto">
				      		</div>
			      		</div>
		      		</div>
		      		<div class="cont-comprar col-sm-12 col-md-12 col-lg-6">
		      			<div class="row">
			      			<div class="col-12 cabezote">
					      		<img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/6.png">
					      		<p>Paga inicialmente el 50% del valor de la compra para proceder a realizar la compra</p>
				      		</div>
				      		<div class="col-12 im-cabezote">
				      			<img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/pago-inicial.png" alt="Pago inicial">
				      		</div>
			      		</div>
		      		</div>
		      	</div>

		      	<div class="row co-pasos pt-5">
		      		<div class="cont-comprar col-sm-12 col-md-12 col-lg-6">
		      			<div class="row">
			      			<div class="col-12 cabezote">
					      		<img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/7.png">
					      		<p>Nosotros te comunicaremos cuando el producto llegue a nuestras oficinas de Bogotá (en 8 a 15 dias - dependiendo el tiempo de entrega del vendedor).</p>
				      		</div>
				      		<div class="col-12 im-cabezote">
				      			<img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/producto-en-oficina.png" alt="producto en oficina">
				      		</div>
			      		</div>
		      		</div>
		      		<div class="cont-comprar col-sm-12 col-md-12 col-lg-6">
		      			<div class="row">
			      			<div class="col-12 cabezote">
					      		<img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/8.png">
					      		<p>Paga el 50% restante del valor de la compra para proceder a realizar el despacho del producto donde nos indicaste (si es fuera de Bogotá), o recogerlo con pago Contra-Entrega en nuestras oficinas en Bogotá.</p>
				      		</div>
				      		<div class="col-12 im-cabezote">
				      			<img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/valor-restante.png" alt="pago restante">
				      		</div>
			      		</div>
		      		</div>
		      	</div>

		      	<div class="row co-pasos pt-5">
		      		<div class="cont-comprar col-sm-12 col-md-12 col-lg-6">
		      			<div class="row">
			      			<div class="col-12 cabezote">
					      		<img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/9.png">
					      		<p>Recibe el producto en menos de 48 horas (si es fuera de Bogotá).</p>
				      		</div>
				      		<div class="col-12 im-cabezote">
				      			<img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/recibe-el-producto.png" alt="pago restante">
				      		</div>				      		
			      		</div>
		      		</div>
		      		<div class="cont-comprar col-sm-12 col-md-12 col-lg-6">
		      			<div class="row">
			      			<div class="col-12 cabezote">
					      		<img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/10.png">
					      		<p>Disfruta de tus ComprasW!!!</p>
				      		</div>
				      		<div class="col-12 im-cabezote">
				      			<img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/disfruta-el-producto.png" alt="Disfruta el producto">
				      		</div>					      		
			      		</div>
		      		</div>
		      	</div>

		      </div>
		      <!-------- end primer tab -------->
		      <!-------- segundo tab -------->
		      <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
		      	<h2>Donde COMPRAR<br>
					(Tiendas Sugeridas)</h2>
				<div class="row tiendas pt-3">
					<a href="https://www.amazon.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-1.jpg" alt="amazon"></a>
					<a href="https://www.ebay.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-2.jpg" alt="ebay"></a>
					<a href="https://www.bestbuy.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-3.jpg" alt="bestbuy"></a>
					<a href="http://www.tigerdirect.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-4.jpg" alt="tigerdirect"></a>
					<a href="https://www.rakuten.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-5.jpg" alt="rakuten"></a>
					<a href="https://es.aliexpress.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-6.jpg" alt="aliexpress"></a>
					<a href="https://www.alibaba.com" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-7.jpg" alt="alibaba"></a>
					<a href="https://es.dhgate.com" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-8.jpg" alt="dhgate"></a>
					<a href="https://www.banggood.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-9.jpg" alt="banggood"></a>
					<a href="https://www.geekbuying.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-10.jpg" alt="geekbuying"></a>
					<a href="https://www.lightinthebox.com/es" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-11.jpg" alt="lightinthebox"></a>
					<a href="https://www.walmart.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-12.jpg" alt="walmart"></a>
					<a href="https://intl.target.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-13.jpg" alt="intl target"></a>
					<a href="http://www.toysrus.es" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-14.jpg" alt="toysrus"></a>
					<a href="https://www.macys.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-15.jpg" alt="macys"></a>
					<a href="https://www.dickssportinggoods.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-16.jpg" alt="dicks sporting"></a>
					<a href="https://www.zappos.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-17.jpg" alt="zappos"></a>
					<a href="https://www.lowes.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-18.jpg" alt="lowes"></a>
					<a href="https://www.homedepot.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-19.jpg" alt="home depot"></a>
					<a href="https://shop.advanceautoparts.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-20.jpg" alt="Advance auto parts"></a>
					<a href="http://www.motorcycle-superstore.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-21.jpg" alt="motorcycle superstore"></a>
					<a href="https://www.interstatemusic.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-22.jpg" alt="Cascio"></a>
					<a href="http://www.musiciansfriend.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-23.jpg" alt="Musician's friend"></a>
					<a href="https://www.americanmusical.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logos/tienda-24.jpg" alt="americanmusical"></a>
				</div>
		      </div>
		      <!-------- end segundo tab -------->
		      <!-------- tercero tab -------->
		      <div class="tab-pane fade form_nav" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
				  <h2>Solicitud de Cotización</h2>
				  <?php echo do_shortcode( '[contact-form-7 id="34" title="Formulario Compras Bajo Pedido"]' ); ?>
		      </div>
		      <!-------- end tercero tab -------->
		      <!-------- cuarto tab -------->
		      <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
		      	<h2>Quienes somos</h2>
		      	<div class="row quienes-somos">
		      		<div class="col-sm-12 col-md-12 col-lg-10 p-4">
		      			<p>ComprasW, es un portal filial de la compañía Weston Parts localizada cerca a Miami, Estados Unidos; que desde 2005 decidió comenzar su relación comercial  con el mercado Latinoamericano ofreciendo la compra y suministros de productos requeridos por nuestros clientes para su producción.</p>

						<p>Teniendo esta infraestructura, ComprasW complementa el ciclo de distribución mediante las COMPRAS POR INTERNET de todo tipo de productos requeridos por los clientes para sus diferentes necesidades y ocasiones.</p>

						<p>ComprasW tiene como objetivo principal, ser un facilitador estratégico que permite a nuestros clientes, comprar los productos a mejor precio que en el mercado local y adquirir los diferentes productos que solo se encuentran en el mercado internacional.</p>

						<p>Por tal razón, permítanos conocer sus necesidades comunicándonos cualquier productos que necesite (computadores, productos electrónicos, celulares, juguetes, repuestos, accesorios, equipos musicales, maquinaria, herramienta, ropa, zapatos, productos deportivos, productos de ferretería, accesorios para carro, para motos, etc.), que con gusto le cotizaremos lo más pronto posible.</p>
		      		</div>
		      		<div class="col-sm-12 col-md-12 col-lg-2">
		      			<img src="<?php echo get_template_directory_uri(); ?>/img/compras-bajo-pedido/logo-weston.jpg" alt="Logo weston">
		      		</div>
		      	</div>
		      </div>
		      <!-------- end cuarto tab -------->
		    </div>
		  </div>
		</div>
	</div>
</div>

<?php get_footer(); ?>