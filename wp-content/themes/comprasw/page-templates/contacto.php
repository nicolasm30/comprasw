<?php
/*=================
Template Name: Contacto
===================*/
get_header('wordpress'); ?>

<div class="container-fluid page-contacto">
    <div class="row">
        <div class="banner_contacto">
            <img src="<?php echo get_template_directory_uri(); ?>/img/Contacto/BANNER-CONTACTO.jpg" alt="">
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Contacto</h1>
            </div>
        </div>
        <div class="row datos_Contacto">
            <div class="col-md-3">
                <div class="box_data">
                    <div class="box_data_img">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/Contacto/ICON-TEL.png" alt="">
                    </div>
                    <div class="box_data_text">
                        <h3>Télefonos Fijos</h3>
                        <p>571 432-4590</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box_data">
                    <div class="box_data_img">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/Contacto/ICON-CEL.png" alt="">
                    </div>
                    <div class="box_data_text">
                        <h3>Teléfonos Móviles</h3>
                        <p>(57) 322-591-9175</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box_data">
                    <div class="box_data_img">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/Contacto/ICON-MAIL.png" alt="">
                    </div>
                    <div class="box_data_text">
                        <h3>Correos Electrónicos</h3>
                        <a href="mailto:ventas@comprasw.com">ventas@comprasw.com</a>
                        <a href="mailto:info@comprasw.com">info@comprasw.com</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box_data">
                    <div class="box_data_img">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/Contacto/ICON-SOCIAL.png" alt="">
                    </div>
                    <div class="box_data_text">
                        <h3>Redes Sociales</h3>
                        <a href="https://www.facebook.com/ComprasW-424650571297371/" target="_blank" rel="noopener noreferrer">Facebook</a>
                        <a href="https://twitter.com/compraswcol" target="_blank" rel="noopener noreferrer">Twitter</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row caja_form">
            <div class="col-md-6 figure_contact">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d106260.95153026578!2d-74.15589042432644!3d4.69810869770323!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e3f9bfd2da6cb29%3A0x239d635520a33914!2zQm9nb3TDoQ!5e0!3m2!1ses-419!2sco!4v1566881217333!5m2!1ses-419!2sco" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>
            <div class="col-md-6 box_form">
                <p>
                    Si necesitas soporte adicional no dudes en llenar el formulario o comunicarte con nosotros
                </p>
                <?php echo do_shortcode( '[contact-form-7 id="31" title="Formulario Page Contacto"]' ); ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>


