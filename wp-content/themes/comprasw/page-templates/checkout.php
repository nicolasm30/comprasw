<?php 
/* 
Template Name: Checkout
*/

?>
<?php get_header('wordpress'); ?>

<div class="container checkout">
    <div class="row">
        <div class="col">
            <?php echo do_shortcode('[woocommerce_checkout]'); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>