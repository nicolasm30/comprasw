    <footer>
        <div class="container">
            <div class="data-footer row">
                <div class="col-md-3">
                    <ul class="contacto_footer">
                        <li>Teléfono: <a href="tel:+5714324590" style="color:#fff;">(571) 432-4590</a></li>
                        <li>Celular: <a href="tel:+573225919175" style="color:#fff;">(57) 322 591-9175</a></li>
                        <li>Correo: <a href="mailto:info@comprasw.com?Subject=Interesado%20en%20el%20producto" style="color:#fff;">info@comprasw.com</a></li>
                        <li>Términos y Condiciones</li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <div class="pago_footer">
                        <p>Formas de pago</p>
                        <img src="<?php echo get_template_directory_uri() ?>/img/Footer/pagosfooter.png" alt="">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="metodos_pago">
                        <div class="row">
                            <div class="col-xs-1 col-md-12 col-lg-12">
                                <div class="row c-redes">
                                    <div class="col-xs-1 col-md-3 col-lg-3">
                                        <a href="#">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/Footer/facebook.png" alt="">
                                        </a>
                                    </div>
                                    <div class="col-xs-1 col-md-3 col-lg-3">
                                        <a href="#">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/Footer/twitter.png" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1 col-md-12 col-lg-12 envio">                             
                                <a href="#">
                                    <img src="<?php echo get_template_directory_uri() ?>/img/Footer/shipped.png" alt="">
                                    <p>Métodos de Envío</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <ul class="copy">
                        <li>ComprasW2019®</li>
                        <li>Todos los Derechos Reservados</li>
                        <li>Desarrollado por: Kboom Estudios</li>
                        <li>Política de Tratamiento de Datos</li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <?php wp_footer(); ?>
</body>

</html>
