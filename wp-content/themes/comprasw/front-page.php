
<?php get_header('wordpress'); ?>

<div class="container-fluid inicio">
	<div class="row">
		<div class="banner-inicio">
			<?php add_revslider('inicio'); ?>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12 titles">
				<h2>Productos Destacados</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="products_destacados owl-carousel">
					<?php
        				$args = array( 'post_type' => 'product', 'posts_per_page' => 8, 'product_cat' => 'destacados');
        				$loop = new WP_Query( $args );
        				while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
          				<li class="product-destacado">
	            			<a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>">
	            				<?php
									if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog');
									else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="300px" height="300px" />';
									the_title( '<h3>', '</h3>' );
	              				?>
	            			</a>
        				</li>
    				<?php endwhile;
    				wp_reset_query(); 
    				?>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 titles">
				<h2>Todos los Productos</h2>
			</div>
		</div>
		<div class="row all-products">
			<div class="col-md-3">
				<?php include (TEMPLATEPATH . '/categories.php'); ?>
			</div>
			<div class="col-md-9">
				<ul class="products">
					
				</ul>
			</div>
		</div>
	</div>

	<!-- solicitud de cotizacion -->
	<div class="row solicitudCotizacion" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/FondoSolicitudCotizacion.png);">
		<div class="container">
			<div class="row">
				<div class="col-xs-1 col-md-12 col-lg-6 coti1">
					<img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/textoSolicitudCotizacion.png" alt="">
					<a href="" target="_blank">conoce más</a>
				</div>
				<div class="col-xs-1 col-md-12 col-lg-6 coti2">
					<div class="title-coti">
						<img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/ico-cotizacion.png" alt="">
						<h3>SOLICITUD DE COTIZACIÓN</h3>
					</div>
					<?php echo do_shortcode( '[contact-form-7 id="333" title="Solicitud de cotización"]' ); ?>
				</div>
			</div>
		</div>
	</div>
	<!-- end solicitud de cotizacion -->

	<!-- suscribete News Letter -->
	<section class="cont-newsletter">
		<div class="container">
			<div class="row">
				<div class="col-xs-1 col-md-12 col-lg-6 news1">
					<img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/suscribete-news.png" alt="">
				</div>
				<div class="col-xs-1 col-md-12 col-lg-6 news2">
					<?php echo do_shortcode( '[contact-form-7 id="335" title="Suscripción News letter"]' ); ?>
				</div>
			</div>
		</div>
	</section>
	<!-- end suscribete News Letter -->

</div>

<?php get_footer(); ?>